import { HomeComponent } from "src/app/home/home.component";


export const APP_ROUTES = [
  { path: '', component: HomeComponent, title: 'Home - Job Search' },
];
