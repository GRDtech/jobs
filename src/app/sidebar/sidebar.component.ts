import { Component,inject, HostListener } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilsService } from 'src/Utils/utils.service';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
  utilService = inject(UtilsService);
  mobileView = true;
  menuToggleValue$ = this.utilService.getMenubarToggle();

  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    if (window.innerWidth <= 880) {
      this.mobileView = true;
    } else {
      this.mobileView = false;
      // this.utilService.setMenubarToggle();
    }
  }

  ngOnInit() {}
  sidebarClose(event: Event) {
    console.log('click outside');
    event.preventDefault();
    this.utilService.setMenubarToggle();
  }
}
