import { Component,inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilsService } from 'src/Utils/utils.service';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  utilService = inject(UtilsService);
  
  onMenubarToggle() {
    this.utilService.setMenubarToggle();
  }

}
