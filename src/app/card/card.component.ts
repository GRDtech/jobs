import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilsService } from 'src/Utils/utils.service';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  utilService = inject(UtilsService)
  onCardClick(){
    this.utilService.setCardToggle()
  }
}
