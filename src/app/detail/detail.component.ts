import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilsService } from 'src/Utils/utils.service';

@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent {
  utilService = inject(UtilsService)
  cardStatus$ = this.utilService.getCardToggle();
  onDetailClose(){
    this.utilService.setCardToggle()
  }
}
