import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UtilsService {
  private readonly menubarToggle = new BehaviorSubject<boolean>(false);
  private readonly menubarToggle$ = this.menubarToggle.asObservable();

  private readonly cardToggle = new BehaviorSubject<boolean>(false);
  private readonly cardToggle$ = this.cardToggle.asObservable();

  setMenubarToggle() {
    let value = !this.menubarToggle.getValue();
    this.menubarToggle.next(value);
  }
  getMenubarToggle(): Observable<boolean> {
    return this.menubarToggle$;
  }
  setCardToggle() {
    let value = !this.cardToggle.getValue();
    this.cardToggle.next(value);
  }
  getCardToggle(): Observable<boolean> {
    return this.cardToggle$;
  }
}
