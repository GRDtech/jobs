// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// import { AppModule } from './app/app.module';


// platformBrowserDynamic().bootstrapModule(AppModule)
//   .catch(err => console.error(err));
import 'zone.js/dist/zone';
import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { bootstrapApplication } from '@angular/platform-browser';
import { provideRouter, RouterOutlet } from '@angular/router';
import { APP_ROUTES } from './Router/app-router';

import { SidebarComponent } from './app/sidebar/sidebar.component';
import { DetailComponent } from './app/detail/detail.component';

@Component({
  selector: 'my-app',
  standalone: true,
  imports: [CommonModule, RouterOutlet, SidebarComponent, DetailComponent],
  templateUrl: 'main.html',
})
export class App {
  name = 'Angular';
}

bootstrapApplication(App, {
  providers: [provideRouter(APP_ROUTES)],
});
